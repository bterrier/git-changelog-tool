#ifndef GIT_H
#define GIT_H

#include <QObject>
#include <QProcess>
#include <QSharedPointer>

class Git : public QObject
{
    Q_OBJECT
public:
    explicit Git(QObject *parent = 0);

    QString binPath() const;
    void setBinPath(const QString &binPath);

    QString repoPath() const;
    void setRepoPath(const QString &repoPath);

    static QString getVersion(const QString &binPath);
signals:

public slots:
    QString commitMessage(const QString &sha1) const;
    QStringList commits(const QString &range) const;

private:

    QSharedPointer<QProcess> makeProcess() const;
    QString m_binPath;
    QString m_repoPath;
};

#endif // GIT_H
