# git changelog tool

This is a toolset that extracts marked changelog entries in git messages.

## Components
The toolset is made of the following components:

* A graphical tool
* A graphical wizard
* A command line interface

The toolset also contains 2 additional libraries:

* A core library that contains the common core logic for the tools.
* A widget library that contains widgets shared by the graphic tools.

## Build

The tool can be built simply by using `qmake`.
For more detailed information about the build process see `INSTALL.md`
