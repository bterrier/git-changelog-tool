#ifndef REPOSELECTPAGE_H
#define REPOSELECTPAGE_H

#include <QWizardPage>

namespace Ui {
class RepoSelectPage;
}

class RepoSelectPage : public QWizardPage
{
    Q_OBJECT

public:
    explicit RepoSelectPage(QWidget *parent = 0);
    ~RepoSelectPage();

private:
    Ui::RepoSelectPage *ui;
};

#endif // REPOSELECTPAGE_H
