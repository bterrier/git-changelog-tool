#include <QApplication>

#include "wizard.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setApplicationVersion(VERSION_STRING);
    QGuiApplication::setApplicationDisplayName("ChangeLog Tool Wizard");

    QApplication a(argc, argv);

    Wizard wizard;
    wizard.show();

    return a.exec();
}
