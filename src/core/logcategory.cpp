#include "logcategory.h"

LogCategory::LogCategory(const QString &name, LogCategory *parent) :
    m_name(name),
    m_parent(parent)
{

}

LogCategory::~LogCategory()
{
	qDeleteAll(m_subs);
}

void LogCategory::append(const QString &message)
{
	m_messages.append(message);
}

LogCategory *LogCategory::find(const QString &name) const
{
	auto it = m_subs.find(name);

	if (it == m_subs.constEnd())
	{
		return nullptr;
	}

	return it.value();

}

LogCategory *LogCategory::findOrAdd(const QString &name)
{
	auto it = m_subs.find(name);

	if (it == m_subs.constEnd())
	{
		LogCategory *sub = new LogCategory(name, this);
		it = m_subs.insert(sub->name(), sub);
	}

	return it.value();
}

QString LogCategory::name() const
{
	return m_name;
}

QStringList LogCategory::messages() const
{
	return m_messages;
}

LogCategory *LogCategory::parent() const
{
	return m_parent;
}
