## Build

This project only depends on Qt.
It is recommanded to do an "out of source" build (or shadow build).

To build execute the following commands:

    qmake -r <path_to_sources>/git-changelog-tools.pro
    make

_Note: Replace `make` by the actual make command for your platform
(`mingw32-make`, `nmake`, `jom`, etc.)_

## Package
### Generate a setup for Microsoft Windows

A Makefile target is available for generating a setup.
The setup is generated using Inno Setup.

    make setup

### Generate a tar archive

A Makefile target is available for generating a portable archive. The default
behavior is to generate a .tar.gz file at the root of the build directory.

    make tarball

_Note: On Linux, the tar will only contain the generated binaries. On Windows,
all Qt dependencies will be included using `windeployqt`, this means that when
using MSVC the `vc_redist.exe` setup will be included and needs to be
installed._

