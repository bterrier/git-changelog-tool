#ifndef AUTOCONFPAGE_H
#define AUTOCONFPAGE_H

#include <QFutureWatcher>
#include <QString>

#include <QWizardPage>

namespace Ui {
class AutoConfPage;
}

class AutoConfPage : public QWizardPage
{
    Q_OBJECT

public:
    explicit AutoConfPage(QWidget *parent = 0);
    ~AutoConfPage();

private slots:
    void onGitVersionReady();

private:
    Ui::AutoConfPage *ui;
    QFutureWatcher<QString> m_watcher;

    // QWizardPage interface
public:
    void initializePage() override;
    bool isComplete() const override;
    void setComplete(bool complete);

private :
    bool m_complete = false;

    // QWizardPage interface
public:
};

#endif // AUTOCONFPAGE_H
