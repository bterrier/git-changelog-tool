#include "cltool.h"

#include <memory>

#include <QCoreApplication>
#include <QRegularExpression>
#include <QStringBuilder>

#include "git.h"
#include "htmlmaker.h"
#include "logcategory.h"
#include "markdownmaker.h"


CLTool::CLTool(QObject *parent) :
    QObject(parent),
    m_git(new Git(this))
{
}

Git *CLTool::git() const
{
    return m_git;
}

QString CLTool::key() const
{
    return m_key;
}

void CLTool::setKey(const QString &key)
{
    m_key = key;
}

/*!
 * \brief Process change logs and fill data structure
 * \param data Pointer to the data structure to be filled
 * \param changeLogs Change logs
 *
 * \note If \a data is NULL, this function does nothing.
 */
void CLTool::extractLogCategories(LogCategory *data, const QStringList &changeLogs)
{
    if (! data)
        return;

    const QString key = '[' % m_key % ']';
    QRegularExpression regexp("\\[([^\\]]*)\\]",
                              QRegularExpression::DotMatchesEverythingOption);
    regexp.optimize();

    for (auto it = changeLogs.constBegin() ; it != changeLogs.constEnd() ; ++it)
    {
        LogCategory *cat = data;
        int pos = key.length();
        auto match = regexp.match(*it, pos, QRegularExpression::NormalMatch, QRegularExpression::AnchoredMatchOption);

        while (match.hasMatch())
        {
            const QString subKey = match.captured(1);
            cat = cat->findOrAdd(subKey);
            pos += match.capturedLength();

            match = regexp.match(*it, pos, QRegularExpression::NormalMatch, QRegularExpression::AnchoredMatchOption);
        }

        const QString message = it->mid(pos);
        cat->append(message);
    }
}

QString CLTool::outputFormat() const
{
    return m_outputFormat;
}

void CLTool::setOutputFormat(const QString &outputFormat)
{
    m_outputFormat = outputFormat;
}

QString CLTool::work(const QString &range)
{
    const QStringList commits = m_git->commits(range);

    QStringList changeLogs;

    for (auto it = commits.constBegin() ; it != commits.constEnd() ; ++it)
    {
        if (it->isEmpty())
            continue;

        const QString message = m_git->commitMessage(*it);
        changeLogs += extractChangeLogs(message);
    }


    std::unique_ptr<LogCategory> data = std::make_unique<LogCategory>("change log");
    extractLogCategories(data.get(), changeLogs);

    std::unique_ptr<AbstractMaker> maker;
    if (m_outputFormat.toCaseFolded() == "html")
    {
        maker = std::make_unique<HtmlMaker>();
    }
    else
    {
        maker = std::make_unique<MarkdownMaker>();
    }
    maker->generate(data.get());

    return maker->string();
}

QStringList CLTool::extractChangeLogs(const QString &commitMessage) const
{
    QStringList changeLogs;
    const QStringList lines = commitMessage.split('\n');
    const QString key = '[' % m_key % ']';

    for (auto lineIt = lines.constBegin() ; lineIt != lines.constEnd() ; ++lineIt)
    {
        if (lineIt->startsWith(key))
        {
            QString changeLog = *lineIt;
            for (++lineIt ; lineIt != lines.constEnd() ; ++lineIt)
            {
                if (!lineIt->isEmpty())
                {
                    changeLog += ' ' % *lineIt;
                }
                else
                {
                    break;
                }
            }

            if (lineIt == lines.constEnd())
            {
                break;
            }
            changeLogs += changeLog;
        }
    }

    return changeLogs;
}

