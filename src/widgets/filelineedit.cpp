#include "filelineedit.h"
#include "ui_filelineedit.h"

#include <QDir>

#include <QFileDialog>

#include <QDebug>

FileLineEdit::FileLineEdit(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FileLineEdit)
{
    ui->setupUi(this);

    connect(ui->pushButton, &QPushButton::clicked, this, &FileLineEdit::onPushButtonClicked);
    connect(ui->lineEdit, &QLineEdit::textChanged, this, &FileLineEdit::pathChanged);

}

FileLineEdit::~FileLineEdit()
{
    delete ui;
}

void FileLineEdit::setPath(const QString &path)
{
    ui->lineEdit->setText(QDir::toNativeSeparators(path));
}

QString FileLineEdit::path() const
{
    return QDir::fromNativeSeparators(ui->lineEdit->text());
}

QString FileLineEdit::nativePath() const
{
    return ui->lineEdit->text();
}

void FileLineEdit::onPushButtonClicked()
{
    QString path = QFileDialog::getExistingDirectory(this, tr("Select directory"), this->path());
    if (! path.isEmpty())
    {
        setPath(path);
    }
}
