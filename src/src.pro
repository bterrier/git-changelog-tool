TEMPLATE = subdirs
SUBDIRS += gui widgets wizard \
    core \
    cli

gui.depends += widgets core
wizard.depends += widgets core
cli.depends += core
