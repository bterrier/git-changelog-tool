#ifndef CLTOOL_H
#define CLTOOL_H

#include <QObject>

class Git;
class LogCategory;

class CLTool : public QObject
{

public:
    CLTool(QObject *parent = nullptr);

    Git *git() const;

    QString key() const;
    void setKey(const QString &key);

    QString work(const QString &range);
    QStringList extractChangeLogs(const QString &commitMessage) const;

    void extractLogCategories(LogCategory *data, const QStringList &changeLogs);

    QString outputFormat() const;
    void setOutputFormat(const QString &outputFormat);

private:
    Git *m_git = nullptr;
    QString m_key;
    QString m_outputFormat;
};

#endif // CLTOOL_H
