#define MSVC_REDIST_32 AddBackslash(APP_DIR_PATH) + "vcredist_x86.exe"
#define MSVC_REDIST_64 AddBackslash(APP_DIR_PATH) + "vcredist_x64.exe"

[Setup]
OutputBaseFilename=git_changelog_tools-{#EXE_VERSION}

; Application info
AppCopyright=Benjamin Terrier
AppName=Git ChangeLog Tools
AppVerName=Git ChangeLog Tools v{#EXE_VERSION}
AppVersion={#EXE_VERSION}
AppPublisher=Benjamin Terrier
AppPublisherURL=https://gitlab.com/users/bterrier

; Setup info
VersionInfoVersion={#EXE_VERSION}
VersionInfoCompany=Benjamin Terrier

DefaultDirName={pf}\git-changelog-tools
DefaultGroupName=Git ChangeLog Tools
ShowLanguageDialog=yes
AppID={{A5B0193C-0D3F-4BAC-B482-06E5C3C13A93}
DisableDirPage=auto
DisableProgramGroupPage=auto

; Windows Vista
MinVersion=6.0.6000

#if APP_ARCH == "x86"                       
#elif APP_ARCH == "x86_64"
ArchitecturesInstallIn64BitMode=x64
ArchitecturesAllowed=x64
#else
# error Unsupported architecture
#endif



SolidCompression=true
Compression=lzma2/ultra64
UseSetupLdr=true

[Languages]
Name: "en"; MessagesFile: "compiler:Default.isl"
Name: "fr"; MessagesFile: compiler:Languages\French.isl

[Files]
Source: {#APP_DIR_PATH}\*.*; DestDir: {app}\bin\; Flags: recursesubdirs createallsubdirs ignoreversion

[Run]
#if FileExists(MSVC_REDIST_32)
Filename: "{app}\bin\vcredist_x86.exe"; Parameters: "/silent /passive /norestart"
#elif FileExists(MSVC_REDIST_64)
Filename: "{app}\bin\vcredist_x64.exe"; Parameters: "/silent /passive /norestart"
#endif

[Icons]
Name: "{group}\Git ChangeLog Tools GUI"; Filename: "{app}\bin\git-changelog-gui.exe" ; IconFilename: "{app}\bin\git-changelog-gui.exe"  
Name: "{group}\Git ChangeLog Tools Wizard"; Filename: "{app}\bin\git-changelog-wizard.exe" ; IconFilename: "{app}\bin\git-changelog-wizard.exe"
Name: "{group}\{cm:UninstallProgram, Git ChangeLog Tools}"; Filename: "{uninstallexe}"

[CustomMessages]
en.UNINSTALL_PREVIOUS=Uninstalling previous release...
fr.UNINSTALL_PREVIOUS=Désinstallation de la version précédente...  


[Code]
//  Function:  GetPreviousUninstall
//    Get the previous installation path.
Function GetPreviousUninstall : String;
Var
  strAppId, strRegKey : String;
Begin
  Result := ''
  
  //  Get app id 
  strAppId := '{#emit SetupSetting("AppId")}';
  if strAppId[2]='{' then
    strAppId := Copy( strAppId, 2, 100 );
    
  //  Search for regular locations...      
  strRegKey := Format( '%s\%s_is1', ['Software\Microsoft\Windows\CurrentVersion\Uninstall', strAppId ] );
  if not RegQueryStringValue(HKEY_LOCAL_MACHINE, strRegKey, 'UninstallString', Result ) then
  Begin
    if not RegQueryStringValue(HKEY_CURRENT_USER, strRegKey, 'UninstallString', Result) then
    Begin
      //  Search for 32-bit apps installed on 64-bit hosts
      strRegKey := Format( '%s\%s_is1', ['Software\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall', strAppId ] );
      RegQueryStringValue(HKEY_LOCAL_MACHINE, strRegKey, 'UninstallString', Result);
    end;
  End;
  
  Result := RemoveQuotes( Result );  
End;

// Function:  CurStepChanged
// Event function called by innosetup when changing the current installation step.
procedure CurStepChanged(CurStep: TSetupStep);
Var
  strPreviousUninstall : String;
  iResultCode : Integer;
  
Begin
  //  When installation process begins, we remove the last release
  if CurStep=ssInstall then
  Begin
    //  Get previous install path
    strPreviousUninstall := GetPreviousUninstall();
    if Length( strPreviousUninstall )>0 then
    Begin
      //  Uninstall it
      WizardForm.StatusLabel.Caption := ExpandConstant( '{cm:UNINSTALL_PREVIOUS}' );
      Exec( strPreviousUninstall, '/VERYSILENT /NORESTART', '', SW_HIDE, ewWaitUntilTerminated, iResultCode );
    End;
  End;
End;
procedure RunDriverInstaller;
var
  ResultCode: Integer;
begin
  if not Exec(ExpandConstant('{app}\Setups\installMuxLight.exe'), '', '', SW_SHOWNORMAL,
    ewWaitUntilTerminated, ResultCode)
  then
    MsgBox('Driver installation failed!' + #13#10 +
      SysErrorMessage(ResultCode), mbError, MB_OK);
end;