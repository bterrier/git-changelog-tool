#include "progresspage.h"
#include "ui_progresspage.h"

#include <QtConcurrent>

#include <cltool.h>
#include <git.h>

QString doWork(QString repo, QString range)
{
    CLTool tool;
    tool.git()->setRepoPath(repo);
    return tool.work(range);
}

ProgressPage::ProgressPage(QWidget *parent) :
    QWizardPage(parent),
    ui(new Ui::ProgressPage)
{
    ui->setupUi(this);
    setCommitPage(true);

    connect(&m_watcher, &QFutureWatcher<QString>::finished, this, &ProgressPage::onFinished);
}

ProgressPage::~ProgressPage()
{
    delete ui;
}

void ProgressPage::onFinished()
{
    QString result = m_watcher.result();
    setField("result", result);
    ui->progressBar->setMaximum(1);
    ui->progressBar->setValue(1);
    setComplete(true);
}

void ProgressPage::setComplete(bool complete)
{
    if (complete == m_complete)
        return;

    m_complete = complete;
    emit completeChanged();
}


void ProgressPage::initializePage()
{
    QString repo = field("git_repo").toString();
    QString range = field("range").toString();
    QFuture<QString> f = QtConcurrent::run(&doWork, repo, range);
    m_watcher.setFuture(f);
}

void ProgressPage::cleanupPage()
{
    ui->plainTextEdit->clear();
    ui->progressBar->setMaximum(0);
    ui->progressBar->setValue(0);
}

bool ProgressPage::isComplete() const
{
    return m_complete;
}
