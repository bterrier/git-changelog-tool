#include "resultpage.h"
#include "ui_resultpage.h"

ResultPage::ResultPage(QWidget *parent) :
    QWizardPage(parent),
    ui(new Ui::ResultPage)
{
    ui->setupUi(this);
    registerField("result", ui->plainTextEdit, "plainText");
}

ResultPage::~ResultPage()
{
    delete ui;
}
