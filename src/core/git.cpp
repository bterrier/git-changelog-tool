#include "git.h"

#include <QProcess>
#include <QStandardPaths>

Git::Git(QObject *parent) : QObject(parent)
{
    m_binPath = QStandardPaths::findExecutable(QStringLiteral("git"));
}

QString Git::binPath() const
{
    return m_binPath;
}

void Git::setBinPath(const QString &binPath)
{
    m_binPath = binPath;
}

QString Git::repoPath() const
{
    return m_repoPath;
}

void Git::setRepoPath(const QString &repoPath)
{
    m_repoPath = repoPath;
}

QString Git::getVersion(const QString &binPath)
{
    QProcess p;
    p.start(binPath, QStringList{"--version"});
    p.waitForFinished();
    return QString::fromLocal8Bit(p.readAllStandardOutput());
}

QString Git::commitMessage(const QString &sha1) const
{
    auto ps = makeProcess();
    
    ps->setArguments(QStringList{"cat-file", "commit", sha1});
    
    ps->start();
    ps->waitForFinished();
    
    return QString::fromUtf8(ps->readAllStandardOutput());
}

QStringList Git::commits(const QString &range) const
{
    
    auto ps = makeProcess();
    
    ps->setArguments(QStringList{"rev-list", range});
    
    ps->start();
    ps->waitForFinished();
    
    return QString::fromUtf8(ps->readAllStandardOutput()).split('\n');
}

QSharedPointer<QProcess> Git::makeProcess() const
{
    auto ps = QSharedPointer<QProcess>::create();
    
    ps->setProgram(m_binPath);
    
    if (! m_repoPath.isEmpty())
        ps->setWorkingDirectory(m_repoPath);
    
    return ps;
}
