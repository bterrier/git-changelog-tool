TEMPLATE = subdirs
SUBDIRS += src

deployqt.commands += windeployqt $${top_builddir}/bin/git-changelog-cli.exe $$escape_expand(\n\t)
deployqt.commands += windeployqt $${top_builddir}/bin/git-changelog-gui.exe $$escape_expand(\n\t)
deployqt.commands += windeployqt $${top_builddir}/bin/git-changelog-wizard.exe 
QMAKE_EXTRA_TARGETS += deployqt

setup.commands += $$shell_path(C:\Program Files (x86)\Inno Setup 5\iscc.exe) /O$$shell_path($${top_builddir}) /DAPP_ARCH=$$QMAKE_TARGET.arch /DEXE_VERSION=$$VERSION /DAPP_DIR_PATH=$$shell_path($${top_builddir}/bin) $$shell_path($${top_srcdir}/setup/setup.iss)
setup.depends += deployqt
QMAKE_EXTRA_TARGETS += setup


tarball.commands += tar -cz --transform \'s,^,git-changelog-tools-$${VERSION}/,\' -f $$shell_path($${top_builddir}/git-changelog-tools-$${VERSION}.tar.gz) -C $$shell_path($${top_builddir}) $$shell_path(bin/)
win32: tarball.depends += deployqt
QMAKE_EXTRA_TARGETS += tarball

