#include <cstdio>

#include <QCommandLineOption>
#include <QCommandLineParser>
#include <QCoreApplication>
#include <QFile>

#include <cltool.h>
#include <git.h>

#include <QDebug>

int main(int argc, char *argv[])
{
    QCoreApplication::setApplicationVersion(VERSION_STRING);
    QCoreApplication a(argc, argv);

    QCommandLineParser parser;
    parser.addHelpOption();
    parser.addVersionOption();

    QCommandLineOption repoOption(QStringList{"repo"},
                                  "Use the git repository at <path>. If not set the working directory is used.",
                                  "path");
    parser.addOption(repoOption);


    QCommandLineOption gitOption(QStringList{"g", "git"},
                                  "Set the git binary used to run commands. If not set the system git is used.",
                                  "git");
    parser.addOption(gitOption);


    QCommandLineOption outOption(QStringList{"o", "output"},
                                  "Output file.",
                                  "file");
    parser.addOption(outOption);


    QCommandLineOption formatOption(QStringList{"f", "format"},
                                  "Output format.\n"
                                  "Supported values are:\n"
                                  "   md (default)\n"
                                  "   html",
                                  "format",
                                   "md");
    parser.addOption(formatOption);



    QCommandLineOption rangeOption(QStringList{"r", "range"},
                                  "Commit range. The syntax must follow git format for ranges.",
                                  "range");
    parser.addOption(rangeOption);

    QCommandLineOption keyOption(QStringList{"k", "key"},
                                  "Key tag. The default is 'ChangeLog'.",
                                  "key", "ChangeLog");
    parser.addOption(keyOption);


    parser.process(a);

    CLTool tool;
    Git *git = tool.git();

    if (parser.isSet(gitOption))
    {
        git->setBinPath(parser.value(gitOption));
    }

    if (parser.isSet(repoOption))
    {
        git->setRepoPath(parser.value(repoOption));
    }

    QString range;
    if (parser.isSet(rangeOption))
    {
        range = parser.value(rangeOption);
    }

    QFile file;
    bool open = false;
    if (parser.isSet(outOption))
    {
        file.setFileName(parser.value(outOption));
        open = file.open(QIODevice::Text | QIODevice::WriteOnly);
    }
    else
    {
        open = file.open(stdout, QIODevice::Text | QIODevice::WriteOnly);
    }

    if (!open)
    {
        qCritical() <<  "Failed to open output file." << endl
                    << file.errorString();
        return -1;
    }

    tool.setKey(parser.value(keyOption));
    tool.setOutputFormat(parser.value(formatOption));

    QString result = tool.work(range);

    file.write(result.toUtf8());
    file.close();

    return 0;
}
