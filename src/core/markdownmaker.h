#ifndef MARKDOWNMAKER_H
#define MARKDOWNMAKER_H

#include <QString>
#include <QTextStream>

#include "abstractmaker.h"

class MarkdownMaker : public AbstractMaker
{
public:
	MarkdownMaker();

	void addTitle(const QString &text, int level);
	void addListElem(const QString &text, int level);
    QString string() override;
private:
	QString m_string;
	QTextStream m_stream;
    int m_level = 0;

    // AbstractMaker interface
public:
    void startList() override;
    void endList() override;
    void addListElement(const QString &text) override;
    void startCategory() override;
    void endCategory() override;
    void addTitle(const QString &text) override;
};

#endif // MARKDOWNMAKER_H
