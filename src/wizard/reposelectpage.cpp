#include "reposelectpage.h"
#include "ui_reposelectpage.h"

#include <QDebug>

RepoSelectPage::RepoSelectPage(QWidget *parent) :
    QWizardPage(parent),
    ui(new Ui::RepoSelectPage)
{
    ui->setupUi(this);

    registerField("git_repo*", ui->widget);

    connect(ui->widget, &FileLineEdit::pathChanged, [&](){
            qDebug() << ui->widget->path() << isComplete();
});

    connect(this, &QWizardPage::completeChanged, [&](){
            qDebug() << isComplete();
});
}

RepoSelectPage::~RepoSelectPage()
{
    delete ui;
}

