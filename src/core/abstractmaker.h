#ifndef ABSTRACTMAKER_H
#define ABSTRACTMAKER_H

class QString;
class LogCategory;

class AbstractMaker
{
public:
    AbstractMaker();
    virtual ~AbstractMaker(){}

    virtual void startDocument(){}
    virtual void endDocument(){}

    virtual void startList() = 0;
    virtual void endList() = 0;
    virtual void addListElement(const QString &text) = 0;

    virtual void startCategory() = 0;
    virtual void endCategory() = 0;
    virtual void addTitle(const QString &text) = 0;

    virtual void generate(LogCategory *category, bool newDocument = true);

    virtual QString string() = 0;
};

#endif // ABSTRACTMAKER_H
