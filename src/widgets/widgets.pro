TEMPLATE = lib
CONFIG += static
QT += core gui widgets
FORMS += \
    filelineedit.ui

HEADERS += \
    filelineedit.h

SOURCES += \
    filelineedit.cpp


CONFIG += c++14
