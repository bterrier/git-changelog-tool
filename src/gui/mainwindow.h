#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QHash>
#include <QMimeDatabase>
#include <QMimeType>

#include <QMainWindow>

namespace Ui {
	class MainWindow;
}
class LogCategory;
class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit MainWindow(QWidget *parent = 0);
	~MainWindow();
	QMimeType currentMime() const;

signals:
	void outputChanged(QMimeType);

private slots:
    void on_pushButton_clicked();

	void genereateMd();

	void on_pushButton_3_clicked();


	void onOutputChanged(QMimeType type);

	void on_pushButton_4_clicked();

	void on_comboBox_currentIndexChanged(int index);
	void setCurrentMime(const QMimeType &currentMime);

	void on_actionSave_triggered();

	void on_actionAboutQt_triggered();

    void on_leGit_editingFinished();

private:
	Ui::MainWindow *ui;
	LogCategory *m_data = nullptr;
	QHash<QMimeType, QString> m_outputs;
	QMimeDatabase m_mimeDatabase;
	QMimeType m_currentMime;
	typedef void (MainWindow::*generator)();
	QHash<QMimeType, generator> m_generators;
	void genereateHtml();
};

#endif // MAINWINDOW_H
