#include "wizard.h"

#include "autoconfpage.h"
#include "progresspage.h"
#include "rangeselectpage.h"
#include "reposelectpage.h"
#include "resultpage.h"
#include "welcomepage.h"

Wizard::Wizard(QWidget *parent) :
    QWizard(parent),
    m_tool(new CLTool(this))
{
    setDefaultProperty("FileLineEdit", "path", SIGNAL(pathChanged()));

    addPage(new WelcomePage(this));
    addPage(new AutoConfPage(this));
    addPage(new RepoSelectPage(this));
    addPage(new RangeSelectPage(this));
    addPage(new ProgressPage(this));
    addPage(new ResultPage(this));

    WizardOptions opts = options();
    opts.setFlag(QWizard::NoBackButtonOnStartPage, true);
    opts.setFlag(QWizard::HaveNextButtonOnLastPage, false);
    setOptions(opts);
}
