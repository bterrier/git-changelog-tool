#ifndef FILELINEEDIT_H
#define FILELINEEDIT_H

#include <QWidget>

namespace Ui {
class FileLineEdit;
}

class FileLineEdit : public QWidget
{
    Q_OBJECT

    Q_PROPERTY(QString path READ path WRITE setPath NOTIFY pathChanged USER true)

public:
    explicit FileLineEdit(QWidget *parent = 0);
    ~FileLineEdit();

    void setPath(const QString &path);
    QString path() const;
    QString nativePath() const;

signals:
    void pathChanged();

private slots:
    void onPushButtonClicked();

private:
    Ui::FileLineEdit *ui;
    QString m_path;
};

#endif // FILELINEEDIT_H
