#include "markdownmaker.h"

MarkdownMaker::MarkdownMaker() :
    m_stream(&m_string)
{

}

void MarkdownMaker::addTitle(const QString &text, int level)
{
	for (int i = 0 ; i < level ; ++i)
	{
		m_stream << '#';
	}
	m_stream << ' ' << text << endl;
}

void MarkdownMaker::addListElem(const QString &text, int level)
{
	for (int i = 0 ; i < level ; ++i)
	{
		m_stream << "  ";
	}
	m_stream << "- " << text << endl;
}

QString MarkdownMaker::string()
{
	m_stream.flush();
	return m_string;
}


void MarkdownMaker::startList(){}

void MarkdownMaker::endList(){}

void MarkdownMaker::addListElement(const QString &text)
{
    m_stream << "- " << text << endl;
}

void MarkdownMaker::startCategory()
{
    m_level += 1;
}

void MarkdownMaker::endCategory()
{
    m_level -= 1;
}

void MarkdownMaker::addTitle(const QString &text)
{
    for (int i = 0 ; i < m_level ; ++i)
    {
        m_stream << '#';
    }
    m_stream << ' ' << text << endl;
}
