#include "abstractmaker.h"

#include <QString>

#include "logcategory.h"

AbstractMaker::AbstractMaker()
{

}



void AbstractMaker::generate(LogCategory *category, bool newDocument)
{
    if (! category)
        return;

    if (newDocument)
        startDocument();

    startCategory();
    addTitle(category->name());

    if (! category->messages().isEmpty())
    {
        startList();

        for (auto it = category->messages().constBegin() ;
             it != category->messages().constEnd() ;
             ++it)
        {
            addListElement(*it);
        }

        endList();
    }

    for (auto it = category->constBegin() ;
         it != category->constEnd() ;
         ++it)
    {
        generate(it.value(), false);
    }

    endCategory();

    if (newDocument)
        endDocument();

}
