#ifndef PROGRESSPAGE_H
#define PROGRESSPAGE_H

#include <QFutureWatcher>
#include <QString>

#include <QWizardPage>

namespace Ui {
class ProgressPage;
}

class ProgressPage : public QWizardPage
{
    Q_OBJECT

public:
    explicit ProgressPage(QWidget *parent = 0);
    ~ProgressPage();

private slots:
    void onFinished();

private:
    void setComplete(bool complete);
    Ui::ProgressPage *ui;
    bool m_complete = false;
    QFutureWatcher<QString> m_watcher;
    // QWizardPage interface
public:
    void initializePage() override;
    void cleanupPage() override;
    bool isComplete() const override;
};

#endif // PROGRESSPAGE_H
