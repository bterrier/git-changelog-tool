TARGET = git-changelog-wizard
TEMPLATE = app
QT += core gui widgets concurrent
CONFIG += C++14

DESTDIR = $${top_builddir}/bin

SOURCES += \
    main.cpp \
    wizard.cpp \
    reposelectpage.cpp \
    rangeselectpage.cpp \
    welcomepage.cpp \
    autoconfpage.cpp \
    progresspage.cpp \
    resultpage.cpp

HEADERS += \
    wizard.h \
    reposelectpage.h \
    rangeselectpage.h \
    welcomepage.h \
    autoconfpage.h \
    progresspage.h \
    resultpage.h

FORMS += \
    reposelectpage.ui \
    rangeselectpage.ui \
    welcomepage.ui \
    autoconfpage.ui \
    progresspage.ui \
    resultpage.ui

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../widgets/release/ -lwidgets
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../widgets/debug/ -lwidgets
else:unix: LIBS += -L$$OUT_PWD/../widgets/ -lwidgets

INCLUDEPATH += $$PWD/../widgets
DEPENDPATH += $$PWD/../widgets

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../widgets/release/libwidgets.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../widgets/debug/libwidgets.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../widgets/release/widgets.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../widgets/debug/widgets.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../widgets/libwidgets.a


win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../core/release/ -lcore
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../core/debug/ -lcore
else:unix: LIBS += -L$$OUT_PWD/../core/ -lcore

INCLUDEPATH += $$PWD/../core
DEPENDPATH += $$PWD/../core

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../core/release/libcore.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../core/debug/libcore.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../core/release/core.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../core/debug/core.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../core/libcore.a
