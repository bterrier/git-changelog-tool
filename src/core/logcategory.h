#ifndef LOGCATEGORY_H
#define LOGCATEGORY_H

#include <QMap>
#include <QString>

class LogCategory
{
public:
	QMap<QString, LogCategory *>::const_iterator constBegin() const { return m_subs.constBegin(); }
	QMap<QString, LogCategory *>::const_iterator constEnd() const { return m_subs.constEnd(); }
	LogCategory(const QString &name, LogCategory *parent = nullptr);
	~LogCategory();
	void append(const QString &message);
	LogCategory *find(const QString &name) const;
	LogCategory *findOrAdd(const QString &name);
	QStringList keys() const { return m_subs.keys(); }

	QString name() const;

	QStringList messages() const;

	LogCategory *parent() const;

private:
    QString m_name;
	LogCategory *m_parent = nullptr;
	QMap<QString, LogCategory *> m_subs;
    QStringList m_messages;
};

#endif // LOGCATEGORY_H
