#ifndef HTMLMAKER_H
#define HTMLMAKER_H

#include <QString>
#include <QTextStream>

#include "abstractmaker.h"

class HtmlMaker : public AbstractMaker
{
public:
	HtmlMaker();

    void startDocument() override;
    void endDocument() override;

    void startList() override;
    void endList() override;
    QString string() override;
private:
	QString m_string;
	QTextStream m_stream;
    int m_level = 0;

    // AbstractMaker interface
public:
    void addListElement(const QString &text) override;
    void startCategory() override;
    void endCategory() override;
    void addTitle(const QString &text) override;
};

#endif // HTMLMAKER_H
