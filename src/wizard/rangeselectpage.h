#ifndef RANGESELECTPAGE_H
#define RANGESELECTPAGE_H

#include <QWizardPage>

namespace Ui {
class RangeSelectPage;
}

class RangeSelectPage : public QWizardPage
{
    Q_OBJECT

public:
    explicit RangeSelectPage(QWidget *parent = 0);
    ~RangeSelectPage();

private:
    Ui::RangeSelectPage *ui;
};

#endif // RANGESELECTPAGE_H
