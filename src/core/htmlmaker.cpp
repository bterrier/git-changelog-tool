#include "htmlmaker.h"

HtmlMaker::HtmlMaker() :
    m_stream(&m_string)
{

}

void HtmlMaker::startDocument()
{
	m_stream << "<html><head></head><body>" << endl;
}

void HtmlMaker::endDocument()
{
	m_stream << "</body></html>" << endl;
}

void HtmlMaker::startList()
{
	m_stream << "<ul>" << endl;
}

void HtmlMaker::endList()
{
	m_stream << "</ul>" << endl;
}


QString HtmlMaker::string()
{
	m_stream.flush();
	return m_string;
}


void HtmlMaker::addListElement(const QString &text)
{
    m_stream << "<li>" << text << "</li>" << endl;
}

void HtmlMaker::startCategory()
{
    m_level += 1;
}

void HtmlMaker::endCategory()
{
    m_level -= 1;
}

void HtmlMaker::addTitle(const QString &text)
{
    m_stream << "<h" << m_level << '>' << text << "</h" << m_level << '>' << endl;
}
