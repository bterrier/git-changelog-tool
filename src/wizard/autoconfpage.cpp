#include "autoconfpage.h"
#include "ui_autoconfpage.h"

#include <QFuture>
#include <QtConcurrent>

#include <git.h>

AutoConfPage::AutoConfPage(QWidget *parent) :
    QWizardPage(parent),
    ui(new Ui::AutoConfPage)
{
    ui->setupUi(this);

    connect(&m_watcher, &QFutureWatcher<QString>::finished, this, &AutoConfPage::onGitVersionReady);
}

AutoConfPage::~AutoConfPage()
{
    delete ui;
}

void AutoConfPage::onGitVersionReady()
{
    QString version = m_watcher.future().result();

    if (!version.isEmpty())
    {
        ui->lblGitVersion->setText(version);
        ui->progressBar->setMaximum(1);
        ui->progressBar->setValue(1);
        setComplete(true);
    }
    else
    {
        ui->lblGitVersion->setText(tr("Failed to detect git version. Please check that you have git in PATH."));
    }
}


bool AutoConfPage::isComplete() const
{
    return m_complete;
}

void AutoConfPage::setComplete(bool complete)
{
    if (m_complete != complete)
    {
        m_complete = complete;
        emit completeChanged();
    }
}


void AutoConfPage::initializePage()
{
    QFuture<QString> future = QtConcurrent::run(&Git::getVersion, QStringLiteral("git"));
    m_watcher.setFuture(future);
}
