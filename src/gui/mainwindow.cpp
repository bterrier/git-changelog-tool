#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDir>
#include <QProcess>
#include <QStandardPaths>
#include <QStringBuilder>
#include <QDebug>
#include <QProgressDialog>
#include <QFileDialog>
#include <QMessageBox>
#include <QQueue>
#include <QMimeDatabase>

#include <QtConcurrent>

#include <git.h>

#include "logcategory.h"
#include "htmlmaker.h"
#include "markdownmaker.h"
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
	ui->setupUi(this);
	ui->wAdvancedOptions->setVisible(false);
    ui->widget->setPath(QDir::currentPath());
    ui->leGit->setText(QDir::toNativeSeparators(QStandardPaths::findExecutable("git")));
    on_leGit_editingFinished();
	struct MimeData {
		QString name;
		generator gen;
	};
	QVector<MimeData> data {
        {"text/x-markdown", &MainWindow::genereateMd},
        {"text/html", &MainWindow::genereateHtml}
	};
	for(auto it = data.constBegin() ; it != data.constEnd() ; ++it)
	{
		QMimeType type = m_mimeDatabase.mimeTypeForName(it->name);
		m_generators.insert(type, it->gen);
		QString comment = type.comment();
		qDebug() << comment << comment.toUtf8().toHex();
		if (comment.isEmpty())
			comment = type.name();
		ui->comboBox->addItem(comment, type.name());
	}
	m_currentMime = m_mimeDatabase.mimeTypeForName(data.first().name);
	connect(this, &MainWindow::outputChanged, this, &MainWindow::onOutputChanged);
	connect(ui->actionQuit, &QAction::triggered, this, &MainWindow::close);
}

MainWindow::~MainWindow()
{
	delete ui;
}

static inline QString niceProcess(const QProcess &process)
{
	QString result = process.program();
	const QStringList args = process.arguments();
	if (! args.isEmpty())
	{
		QChar separator(' ');
		result += separator % args.join(separator);
	}

	return result;
}

void MainWindow::on_pushButton_clicked()
{
	QApplication::setOverrideCursor(Qt::BusyCursor);
	setEnabled(false);

	QProgressDialog dialog(this);
	dialog.setWindowModality(Qt::ApplicationModal);
	dialog.setModal(true);
	dialog.setMinimum(0);
	dialog.setMaximum(0);
	dialog.setLabelText(tr("Retrieving commit list..."));
    dialog.resize(480, 120);
    dialog.show();

	QApplication::processEvents();

    QIODevice::OpenMode openMode = QIODevice::ReadOnly|QIODevice::Text;
	QProcess p;
	p.setProcessChannelMode(QProcess::SeparateChannels);
	p.setProgram(ui->leGit->text());
	p.setArguments(QStringList{"rev-list", ui->leRange->text()});
    p.setWorkingDirectory(ui->widget->path());
    p.start(openMode);
	p.waitForFinished();
	if (p.exitStatus() != QProcess::NormalExit || p.exitCode() != 0)
	{
		QApplication::restoreOverrideCursor();
		setEnabled(true);
		dialog.hide();
		QString errorString = tr("Error while retrieving commit list.\n"
		                         "Command:\n"
		                         "    %1\n"
		                         "Message:\n"
		                         "    %2"
		                         ).arg(niceProcess(p),
		                               QString::fromLocal8Bit(p.readAllStandardError()));
		QMessageBox::critical(this, "Error", errorString);
		return;

	}

	QByteArray error = p.readAllStandardError();
	if (! error.isEmpty())
	{
		qWarning() << error.constData();
	}
	const QByteArrayList commits = p.readAllStandardOutput().split('\n');

	const int count = commits.count();
	int currentCommit = 0;
	dialog.setMaximum(count);
	QStringList changeLogs;

	const QString key = '[' % ui->leKeyword->text() % ']';
	for (auto it = commits.constBegin() ; it != commits.constEnd() ; ++it)
	{
		if (it->isEmpty())
			continue;

		p.setArguments(QStringList{"cat-file", "commit" ,*it});
        p.start(openMode);
		p.waitForFinished();
		error = p.readAllStandardError();
		if (! error.isEmpty())
		{
			qWarning() << p.program() << p.arguments() << endl
			           << error.constData();
		}
		const QStringList lines = QString::fromUtf8(p.readAllStandardOutput()).split('\n');

		for (auto lineIt = lines.constBegin() ; lineIt != lines.constEnd() ; ++lineIt)
		{
			if (lineIt->startsWith(key))
			{
				QString changeLog = *lineIt;
				for (++lineIt ; lineIt != lines.constEnd() ; ++lineIt)
				{
					if (!lineIt->isEmpty())
					{
						changeLog += ' ' % *lineIt;
					}
					else
					{
						break;
					}
				}

				if (lineIt == lines.constEnd())
				{
					break;
				}
				changeLogs += changeLog;
			}
		}

		currentCommit += 1;

		dialog.setLabelText(tr("Retrieve commit message %1 of %2").arg(currentCommit).arg(count));

		dialog.setValue(currentCommit);

		QApplication::processEvents();

        if (dialog.wasCanceled())
        {
            QApplication::restoreOverrideCursor();
            setEnabled(true);
            return;
        }

	}
	QRegularExpression regexp("\\[([^\\]]*)\\]",
	                          QRegularExpression::DotMatchesEverythingOption);
	regexp.optimize();
	delete m_data;
	m_data = new LogCategory("change log");

	dialog.setLabelText(tr("Extracting ChangeLog entries..."));
	for (auto it = changeLogs.constBegin() ; it != changeLogs.constEnd() ; ++it)
	{
		LogCategory *cat = m_data;
		int pos = key.length();
		auto match = regexp.match(*it, pos, QRegularExpression::NormalMatch, QRegularExpression::AnchoredMatchOption);

		qDebug() << *it;
		while (match.hasMatch())
		{
			const QString subKey = match.captured(1);
			cat = cat->findOrAdd(subKey);
			pos += match.capturedLength();

			match = regexp.match(*it, pos, QRegularExpression::NormalMatch, QRegularExpression::AnchoredMatchOption);
		}

		const QString message = it->mid(pos);
		cat->append(message);
		QApplication::processEvents();
	}


	dialog.setLabelText(tr("Generate output..."));
	QApplication::processEvents();
	on_pushButton_4_clicked();

	QApplication::restoreOverrideCursor();
	setEnabled(true);

}

void MainWindow::genereateHtml()
{
    HtmlMaker html;
    html.generate(m_data);
    QMimeType mimeType = QMimeDatabase().mimeTypeForName("text/html");
	m_outputs.insert(mimeType, html.string());
	emit outputChanged(mimeType);
}


void MainWindow::genereateMd()
{
	MarkdownMaker md;
    md.generate(m_data);
	QMimeType mimeType = QMimeDatabase().mimeTypeForName("text/x-markdown");
	m_outputs.insert(mimeType, md.string());
	emit outputChanged(mimeType);
}

void MainWindow::on_pushButton_3_clicked()
{
	bool next = ! ui->wAdvancedOptions->isVisible();
	ui->wAdvancedOptions->setVisible(next);
	if (next)
	{
		ui->pushButton_3->setText("-");
	}
	else
	{
		ui->pushButton_3->setText("+");
	}
}



void MainWindow::onOutputChanged(QMimeType type)
{
	if (type == m_currentMime)
	{
		ui->plainTextEdit->setPlainText(m_outputs[type]);
	}
}

void MainWindow::on_pushButton_4_clicked()
{
	generator g = m_generators[m_currentMime];
	if (g)
	{
		(this->*g)();
	}
}

void MainWindow::on_comboBox_currentIndexChanged(int index)
{
    QString typeName = ui->comboBox->itemData(index).toString();
	setCurrentMime(m_mimeDatabase.mimeTypeForName(typeName));
}

QMimeType MainWindow::currentMime() const
{
	return m_currentMime;
}

void MainWindow::setCurrentMime(const QMimeType &currentMime)
{
	if (m_currentMime != currentMime)
	{
		m_currentMime = currentMime;
		ui->plainTextEdit->setPlainText(m_outputs[m_currentMime]);
	}
}

void MainWindow::on_actionSave_triggered()
{
    const QString path = QFileDialog::getSaveFileName(this, tr("Save..."), QString(), m_currentMime.filterString());

	if (! path.isEmpty())
	{
		QFile file(path);

		if (!file.open(QFile::WriteOnly|QFile::Text))
		{
			return;
		}
		const QByteArray data = m_outputs[m_currentMime].toUtf8();
		file.write(data);

		file.close();
	}
}

void MainWindow::on_actionAboutQt_triggered()
{
    qApp->aboutQt();
}

void MainWindow::on_leGit_editingFinished()
{
    QString path = ui->leGit->text();
    QFuture<QString> future = QtConcurrent::run([path]()->QString{ return Git::getVersion(path); });
    auto *watcher = new QFutureWatcher<QString>(this);
    watcher->setFuture(future);

    connect(watcher, &QFutureWatcherBase::finished, [this, watcher](){
            ui->lblGitVersion->setText(watcher->result());
    });
}
