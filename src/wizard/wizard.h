#ifndef WIZARD_H
#define WIZARD_H

#include <QWizard>

#include "cltool.h"

class Wizard : public QWizard
{
    Q_OBJECT
public:
    explicit Wizard(QWidget *parent = 0);

signals:

public slots:

private:
    CLTool *m_tool;
};

#endif // WIZARD_H
