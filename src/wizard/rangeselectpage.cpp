#include "rangeselectpage.h"
#include "ui_rangeselectpage.h"

RangeSelectPage::RangeSelectPage(QWidget *parent) :
    QWizardPage(parent),
    ui(new Ui::RangeSelectPage)
{
    ui->setupUi(this);
    registerField("range*", ui->lineEdit);
    setCommitPage(true);
}

RangeSelectPage::~RangeSelectPage()
{
    delete ui;
}
